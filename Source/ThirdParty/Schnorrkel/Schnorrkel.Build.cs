﻿using System;
using System.Diagnostics;
using System.IO;
using UnrealBuildTool;

public class Schnorrkel : ModuleRules
{
	public Schnorrkel(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;

		bUseRTTI = true;
		CppStandard = CppStandardVersion.Latest;

		if (!LoadSchnorrkel(Target))
		{
			throw new BuildException("Failed to load libschnorrkel");
		}
	}

	private string ModulePath
	{
		get { return ModuleDirectory; }
	}

	private string ThirdPartyPath
	{
		get
		{
			return Path.GetFullPath(Path.Combine(ModulePath,
				"../../ThirdParty/"));
		}
	}

	private static string[] GetSchnorrkelLibraries()
	{
		return new[] {"schnorrkel_crust"};
	}

	private string CreateCMakeBuildCommand(string buildDirectory,
		string buildType)
	{
		const string program = "cmake.exe";

		var currentDir = ModulePath;
		var installPath = Path.Combine(ThirdPartyPath, "Generated/");
		var sourceDir = Path.Combine(currentDir, "Schnorrkel");
		var compiler = "\"Visual Studio 16 2019\"";
		var arguments = " -G " + compiler +
		                " -S " + sourceDir +
		                " -B " + buildDirectory +
		                " -A x64 " +
		                " -T host=x64" +
		                " -DCMAKE_BUILD_TYPE=" + buildType +
		                " -DCMAKE_INSTALL_PREFIX=" + installPath +
		                " -DBOOST_ROOT='" + GetBoostIncludePath() + "'";

		return program + arguments;
	}

	private string CreateCMakeInstallCommand(string buildDirectory,
		string buildType)
	{
		return "cmake.exe --build " + buildDirectory +
		       " --target install --config " + buildType;
	}

	private bool BuildSchnorrkel(ReadOnlyTargetRules Target)
	{
		const string buildType = "Release";

		const string buildDirectory = "schnorrkel-build-" + buildType;
		var buildPath = Path.Combine(ThirdPartyPath,
			"Generated",
			buildDirectory);

		var configureCommand = CreateCMakeBuildCommand(buildPath,
			buildType);
		var configureCode = ExecuteCommandSync(configureCommand);
		if (configureCode != 0)
		{
			Console.WriteLine(
				"Cannot configure CMake project. Exited with code: "
				+ configureCode);
			return false;
		}

		var installCommand = CreateCMakeInstallCommand(buildPath,
			buildType);
		var buildCode = ExecuteCommandSync(installCommand);
		if (buildCode == 0) return true;
		Console.WriteLine("Cannot build project. " +
		                  "Exited with code: " + buildCode);
		return false;
	}

	private string GetBoostIncludePath()
	{
		return "D:\\Programs\\boost_1_75_0";
	}

	private bool LoadSchnorrkel(ReadOnlyTargetRules Target)
	{
		if (Target.Platform != UnrealTargetPlatform.Win64)
		{
			throw new BuildException("Platform must be Win64");
		}
		
		if (!BuildSchnorrkel(Target))
		{
			throw new BuildException("Schnorrkel build failed");
		}

		PublicIncludePaths.Add(GetBoostIncludePath());

		var librariesPath = Path.Combine(ThirdPartyPath,
			"Generated", "lib");

		foreach (var libName in GetSchnorrkelLibraries())
		{
			var libraryPath = Path.Combine(librariesPath,
				libName + ".lib");
			PublicAdditionalLibraries.Add(libraryPath);
		}
		PublicAdditionalLibraries.Add("userenv.lib");

		PublicIncludePaths.Add(Path.Combine(ThirdPartyPath,
			"Generated", "include"));

		return true;
	}

	private int ExecuteCommandSync(string command)
	{
		Console.WriteLine("Running: " + command);
		var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command)
		{
			CreateNoWindow = true,
			UseShellExecute = false,
			RedirectStandardError = true,
			RedirectStandardOutput = true,
			WorkingDirectory = ModulePath
		};

		var p = Process.Start(processInfo);
		if (p == null) return -1;
		p.OutputDataReceived += (_, args) => Console.WriteLine(args.Data);
		p.BeginOutputReadLine();
		p.WaitForExit();

		if (p.ExitCode != 0)
		{
			Console.Write(p.StandardError.ReadToEnd());
		}

		return p.ExitCode;
	}
}