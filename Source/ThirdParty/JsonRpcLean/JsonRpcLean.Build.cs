﻿using System;
using System.IO;
using UnrealBuildTool;

public class JsonRpcLean : ModuleRules
{
	public JsonRpcLean(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "jsonrpc-lean/include"));
	}
}