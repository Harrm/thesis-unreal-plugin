﻿using System;
using System.Diagnostics;
using System.IO;
using UnrealBuildTool;

public class Blake2 : ModuleRules
{
	public Blake2(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;
		bUseRTTI = true;
		CppStandard = CppStandardVersion.Latest;

		PublicIncludePaths.AddRange(new[]{ Path.Combine(ModuleDirectory) });

        PublicAdditionalLibraries.AddRange(
	        new[]{ Path.Combine(ModuleDirectory, "blake2/blake2b.o"), Path.Combine(ModuleDirectory, "blake2/blake2s.o")}
	        );
	}
}