using System;
using System.IO;
using UnrealBuildTool;

public class RapidJson : ModuleRules
{
	public RapidJson(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "rapidjson/include"));
	}
}