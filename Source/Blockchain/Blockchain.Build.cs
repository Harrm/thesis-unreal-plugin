﻿using System.IO;
using UnrealBuildTool;

public class Blockchain : ModuleRules
{
	public Blockchain(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoPCHs;
		
		bEnableExceptions = true;
		bUseRTTI = true;
		CppStandard = CppStandardVersion.Latest;

		PublicDependencyModuleNames.AddRange(
			new[]
			{
				"Core",
				"VaRest",
				"Scale"
			}
		);

		PrivateDependencyModuleNames.AddRange(
			new[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"JsonRpcLean",
				"RapidJson",
				"WebSockets",
				"VaRest",
				"Json",
				"Scale",
				"Schnorrkel",
				"Blake2"
			}
		);
	}
}