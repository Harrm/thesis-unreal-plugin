// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "VaRestJsonValue.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types/GameplayPallet.h"

#include "BlockchainMetodsFunctionLibrary.generated.h"

UCLASS()
class BLOCKCHAIN_API UBlockchainMetodsFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	using Extrinsic = TArray<uint8>;

	UFUNCTION(BlueprintCallable)
	static UVaRestJsonValue* CreateEchoMethodParameters(FString Message);

	UFUNCTION(BlueprintCallable)
	static TArray<uint8> CreateTransferExtrinsic(FString Receiver, int Amount, int32 Nonce);

	UFUNCTION(BlueprintCallable)
	static UVaRestJsonValue* CreateSubmitAndWatchParameters(TArray<uint8> Extrinsic);

	UFUNCTION(BlueprintCallable)
	static FGameplayDataInstance CreateDataInstance_Number(int32 Number);

	UFUNCTION(BlueprintCallable)
	static FGameplayDataInstance CreateDataInstance_String(FString const& String);

	UFUNCTION(BlueprintCallable)
	static FGameplayDataInstance CreateDataInstance_Struct(FGameplayStructInstance Struct);
};
