﻿#pragma once

#include "Components/ActorComponent.h"

#include "Blockchain/Public/Types/GameplayPallet.h"

#include "BlockchainDataComponent.generated.h"

UCLASS(Blueprintable)
class UBlockchainDataComponent: public UActorComponent
{	
	GENERATED_BODY()

public:

	virtual void OnRegister() override;

	ActorId GetTrackedId() const
	{
		return TrackedActorId;
	}
	
	UFUNCTION(BlueprintCallable)
	void SetTrackedActorId(int Id);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnParamUpdate(UParamEntry* Param);

private:
	ActorId TrackedActorId {};
};
