﻿#pragma once

#include "Containers/Union.h"
#include "Misc/TVariant.h"

#include "UnrealScaleCodecs.h"

THIRD_PARTY_INCLUDES_START
#include "scale/scale.hpp"
THIRD_PARTY_INCLUDES_END

#include "GameplayPallet.generated.h"

using StructureId = uint32;

using StructInstanceId = uint32;
struct FGameplayDataInstance;

USTRUCT(BlueprintType)
struct FGameplayStructInstance
{
	GENERATED_BODY()
 
	UPROPERTY(BlueprintReadOnly)
	int32 TypeId;

	UPROPERTY(BlueprintReadOnly)
	int32 Id;

	TArray<TPair<FString, FGameplayDataInstance>> Values;
};

template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& operator<<(Stream& Encoder, const FGameplayStructInstance& StructInstance)
{
	return Encoder << StructInstance.TypeId << StructInstance.Id << StructInstance.Values;
}

UENUM(BlueprintType)
enum class EDataType: uint8 { Number, String, Structure };

// can be created in BP via a method of BlockchainMethodsFunctionLibrary
USTRUCT(BlueprintType)
struct FGameplayDataInstance
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	EDataType Type;
	
	TVariant<int32, TArray<uint8>, FGameplayStructInstance> Data;
};

template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& operator<<(Stream& Encoder, const FGameplayDataInstance& DataInstance)
{
	Encoder << static_cast<uint8_t>(DataInstance.Type);
	if (DataInstance.Type == EDataType::Structure) Encoder << DataInstance.Data.Get<FGameplayStructInstance>().TypeId;
	return Encoder << DataInstance.Data;
}

using ActorId = uint32;
using ActorTypeId = uint32;

UCLASS(BlueprintType, Abstract)
class UGameplayActionBase: public UObject
{
	GENERATED_BODY()

public:
	virtual uint8 GetIndex() const
	{
		unimplemented()
		return -1;
	}
	
	virtual void Encode(kagome::scale::ScaleEncoderStream& Encoder) const
	{
		unimplemented()
	}
};

template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& operator<<(Stream& S, const UGameplayActionBase& Action)
{
	S << Action.GetIndex();
	Action.Encode(S);
	return S;
}


UCLASS(BlueprintType)
class UIdentityAction : public UGameplayActionBase
{
	GENERATED_BODY()

public:
	virtual uint8 GetIndex() const override
	{
		return 0;	
	}

	virtual void Encode(kagome::scale::ScaleEncoderStream& Encoder) const override
	{
		return;
	}
};

UCLASS(BlueprintType)
class USetFieldAction : public UGameplayActionBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString FieldName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FGameplayDataInstance NewValue;

	virtual uint8 GetIndex() const override
	{
		return 1;	
	}

	virtual void Encode(kagome::scale::ScaleEncoderStream& Encoder) const override
	{
		Encoder << FieldName.GetCharArray() << NewValue;
	}
};

UCLASS(BlueprintType)
class UAlterFieldAction : public UGameplayActionBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString FieldName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FGameplayDataInstance DeltaValue;

	virtual uint8 GetIndex() const override
	{
		return 2;	
	}

	virtual void Encode(kagome::scale::ScaleEncoderStream& Encoder) const override
	{
		Encoder << FieldName.GetCharArray() << DeltaValue;
	}
};


UCLASS(BlueprintType)
class UParamEntry: public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString Name;

	FGameplayDataInstance Value;

	UFUNCTION(BlueprintCallable)
	EDataType GetType() const
	{
		return Value.Type;
	}
	
	UFUNCTION(BlueprintCallable)
	int32 GetNumber() const
	{
		ensureAlways(Value.Type == EDataType::Number);
		return *Value.Data.TryGet<int32>();
	}

	UFUNCTION(BlueprintCallable)
	TArray<uint8> GetByteString() const
	{
		ensureAlways(Value.Type == EDataType::String);
		return *Value.Data.TryGet<TArray<uint8>>();
	}

	UFUNCTION(BlueprintCallable)
	FGameplayStructInstance GetStruct() const
	{
		ensureAlways(Value.Type == EDataType::Structure);
		return *Value.Data.TryGet<FGameplayStructInstance>();
	}
};


USTRUCT(BlueprintType)
struct FGameplayActor
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 Id;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 TypeId;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	FString Name;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<UParamEntry*> Params;
};

template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& operator<<(Stream& Encoder, const FGameplayActor& Actor)
{
	return Encoder << /*Actor.Id << Actor.TypeId << */TCHAR_TO_ANSI(*Actor.Name) /*<< Actor.Params;*/;
}

using SessionId = uint32_t;
