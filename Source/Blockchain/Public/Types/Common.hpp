﻿#pragma once

#include <array>

namespace Blockchain::Common
{
	using FHash256 = std::array<uint8_t, 32>;
	using FBlockHash = FHash256;
}
