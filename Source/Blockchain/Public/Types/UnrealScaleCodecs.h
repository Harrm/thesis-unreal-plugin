﻿#pragma once

#include "Containers/UnrealString.h"
#include "Containers/Map.h" 
#include "Misc/TVariant.h"

template <class Stream, class T1, class T2, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& operator<<(Stream& Encoder, const TPair<T1, T2>& Pair)
{
	return Encoder << Pair.Key << Pair.Value;
}

template <class Stream, class T, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& operator<<(Stream& Encoder, const TArray<T>& Array)
{
	Encoder << static_cast<uint32_t>(Array.Num());
	for (auto const& Elem : Array)
	{
		Encoder << Elem;
	}
	return Encoder;
}

template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& operator<<(Stream& Encoder, const FString& String)
{
	return Encoder << String.GetCharArray();
}

template <class Stream, typename VariantType>
Stream& EncodeVariant(Stream& Encoder, const VariantType&)
{
	checkNoEntry()
	return Encoder;
}

template <class Stream, typename VariantType, typename Current, typename... Rest, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& EncodeVariant(Stream& Encoder, const VariantType& Variant)
{
	if (auto ValuePtr = Variant.template TryGet<Current>(); ValuePtr != nullptr)
	{
		return Encoder << *ValuePtr;
	}
	return EncodeVariant<Stream, VariantType, Rest...>(Encoder, Variant);
}


template <class Stream, typename... Variants, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream& operator<<(Stream& Encoder, const TVariant<Variants...>& Variant)
{
	return EncodeVariant<Stream, TVariant<Variants...>, Variants...>(Encoder, Variant);
}
