﻿#pragma once

#include "BlueprintableFuture.h"

#include <VaRestJsonObject.h>
#include <VaRestJsonValue.h>

#include "Types/GameplayPallet.h"

#include "GetActors.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnGetActorsSuccess, TArray<FGameplayActor>, Actors);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnGetActorsFailed, FString const&, ErrorMessage);

UCLASS(BlueprintType)
class UGetActorsFuture: public UObject, public IBlueprintableFuture
{
	GENERATED_BODY()

public:

	virtual void SetReady(UVaRestJsonValue* Result) override
	{
		TArray<FGameplayActor> Actors;
		for (auto& ActorJson: Result->AsArray())
		{
			FGameplayActor Actor {
			.Id = static_cast<int32>(ActorJson->AsObject()->GetNumberField("id")),
			.TypeId = static_cast<int32>(ActorJson->AsObject()->GetNumberField("type_id")),
			.Name = ActorJson->AsObject()->GetStringField("name"),
			.Params {}, // TODO: parse params
			};
			Actors.Push(MoveTemp(Actor));
		}
		OnSuccess.ExecuteIfBound(MoveTemp(Actors));
	}

	virtual void SetFailed(FString const& ErrorMessage) override
	{
		(void)OnFailed.ExecuteIfBound(ErrorMessage);
	}


private:
	FOnGetActorsSuccess OnSuccess;
	FOnGetActorsFailed OnFailed;	
};