﻿#pragma once

#include <UObject/Interface.h>

#include "BlueprintableFuture.generated.h"

UINTERFACE(Blueprintable)
class UBlueprintableFuture: public UInterface
{
	GENERATED_BODY()	
};

class IBlueprintableFuture
{
	GENERATED_BODY()

public:
	virtual void SetReady(class UVaRestJsonValue* Result) = 0;

	virtual void SetFailed(FString const& ErrorMessage) = 0;
};

