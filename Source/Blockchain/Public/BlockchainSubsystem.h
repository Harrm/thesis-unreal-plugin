﻿#pragma once

#include "Subsystems/GameInstanceSubsystem.h"
#include "Async/Future.h"

#include "VaRestJsonObject.h"

#include "Types/GameplayPallet.h"
#include "Types/Common.hpp"
#include "JsonRpcClient.h"
#include "Futures/GetActors.h"
#include "BlockchainDataComponent.h"

#include "BlockchainSubsystem.generated.h"

namespace Blockchain
{
	namespace Metadata
	{
		struct Metadata;
	}
}


DECLARE_DYNAMIC_DELEGATE(FOnWebsocketConnected);

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnWebsocketConnectionError, const FString&, ErrorMessage);

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnJsonRpcResponse, UVaRestJsonValue*, Response);

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnJsonRpcError, const FString&, ErrorMessage);

UCLASS()
class BLOCKCHAIN_API UBlockchainSubsystem final : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UBlockchainSubsystem();

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	UFUNCTION(BlueprintCallable, meta = (Category = "Blockchain|Subsystem"))
	UGetActorsFuture* GetActors();

	UFUNCTION(BlueprintCallable, meta = (Category = "Blockchain|Subsystem"))
	void SpawnActor(FGameplayActor Actor);

	UFUNCTION(BlueprintCallable, meta = (Category = "Blockchain|Subsystem"))
	void ApplyAction(UGameplayActionBase* ActionClass, int32 TargetId);

	UFUNCTION(BlueprintCallable, meta = (Category = "Blockchain|Subsystem"))
	void DestroyActor(int32 ActorId);

	UFUNCTION(BlueprintCallable, meta = (Category = "Blockchain|Subsystem"))
	void ConnectTo(FString Host, int32 Port, const FOnWebsocketConnected& OnConnected,
	               const FOnWebsocketConnectionError& OnError);

	UFUNCTION(BlueprintCallable, meta = (Category = "Blockchain|Subsystem"))
	void CallMethod(FString Module, FString Method, UVaRestJsonValue* Params,
	                const FOnJsonRpcResponse& OnResponse,
	                const FOnJsonRpcError& OnRpcError);

	UFUNCTION(BlueprintCallable, meta = (Category = "Blockchain|Subsystem"))
	bool IsConnected() const;

	void RegisterBlockchainData(UBlockchainDataComponent* Component);
	
private:
	void SubmitExtrinsic(std::vector<uint8> EncodedCall);
	
	uint32 AccumulatedNonce = 0;
	TFuture<Blockchain::Metadata::Metadata> FetchMetadata() const;
	TFuture<Blockchain::Common::FHash256> FetchGenesisHash() const;
	TFuture<uint32> FetchNextAccountNonce(FAnsiStringView AccountSs58) const;

	TUniquePtr<FJsonRpcClient> RpcClient;

	TArray<TWeakObjectPtr<UBlockchainDataComponent>> KnownBlockchainDataComponents;
};
