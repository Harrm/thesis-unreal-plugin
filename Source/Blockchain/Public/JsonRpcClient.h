﻿#pragma once

#include "Async/Future.h"
#include "Misc/TVariant.h"
#include "Templates/ValueOrError.h"
#include "UObject/Object.h"
#include "VaRest/Public/VaRestJsonValue.h"

class IWebSocket;

namespace jsonrpc
{
	class Client;
	class FormatHandler;
	class Value;
}

class BLOCKCHAIN_API FJsonRpcClient
{

public:
	FJsonRpcClient();
	FJsonRpcClient(const FJsonRpcClient&) = delete;
	FJsonRpcClient& operator=(const FJsonRpcClient&) = delete;

	~FJsonRpcClient();
	
	using FCallMethodResponse = TSharedPtr<FJsonValue>;
	using FCallMethodResult = TValueOrError<FCallMethodResponse, FString>;
	using FConnectResult = TValueOrError<FEmptyVariantState, FString>;
	
	TFuture<TSharedPtr<FConnectResult>> Connect(FString Host, int32 Port);

	TFuture<TSharedPtr<FCallMethodResult>> CallMethod(
		FString Module, FString Method, UVaRestJsonValue* Params);

	void AddEventHandler(FString EventKey, TFunction<void(UVaRestJsonValue*)> Handler);
	
	void Close();

	bool IsConnected() const;

private:
	void ProcessResponse(const FString& Message) const;
	static jsonrpc::Value ConvertFJsonToJsonRpcJson(const FJsonValue& Value);
	static TSharedPtr<FJsonValue> ConvertJsonRpcJsonToFJsonValue(const jsonrpc::Value& Value);

	using RpcCallId = uint32;
	RpcCallId NextCallId = 0;
	mutable TArray<TUniquePtr<TPromise<TSharedPtr<FCallMethodResult>>>> CallMethodResultPromises;
	TArray<TFunction<void(UVaRestJsonValue*)>> EventHandlers; 
	
	TSharedPtr<IWebSocket> WebSocket;
	TSharedPtr<jsonrpc::Client> RpcClient;
	TSharedPtr<jsonrpc::FormatHandler> FormatHandler;
};
