﻿#include "BlockchainSubsystem.h"

#include "BlockchainDefines.hpp"
#include "BlockchainMetodsFunctionLibrary.h"
#include "VaRestSubsystem.h"
#include "Async/AsyncResult.h"
#include "Engine/Engine.h"
#include "Types/SubstrateTypes.h"
#include "Types/Metadata.hpp"

UBlockchainSubsystem::UBlockchainSubsystem():
	UGameInstanceSubsystem{}
{
}

void UBlockchainSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	UE_LOG(LogBlockchain, Log, TEXT("Blockchain subsystem initialized"));
}

void UBlockchainSubsystem::Deinitialize()
{
	Super::Deinitialize();
}

UGetActorsFuture* UBlockchainSubsystem::GetActors()
{
	UGetActorsFuture* Promise = NewObject<UGetActorsFuture>(this);

	FOnJsonRpcResponse Delegate;
	Delegate.BindDynamic(Promise, &UGetActorsFuture::SetReady);

	FOnJsonRpcError ErrorDelegate;
	ErrorDelegate.BindDynamic(Promise, &UGetActorsFuture::SetFailed);

	auto VaRestSubsystem = GEngine->GetEngineSubsystem<UVaRestSubsystem>();
	CallMethod("gameplay", "getActors",
	           VaRestSubsystem->ConstructJsonValueArray({VaRestSubsystem->ConstructJsonValueNumber(0)}),
	           Delegate,
	           ErrorDelegate);
	return Promise;
}

void UBlockchainSubsystem::SpawnActor(FGameplayActor Actor)
{
	auto Call = SubstrateTypes::Call<SessionId, FGameplayActor>{8, 5, {0, MoveTemp(Actor)}};
	SubmitExtrinsic(kagome::scale::encode(Call).value());
}

void UBlockchainSubsystem::ApplyAction(UGameplayActionBase* ActionClass, int32 TargetId)
{
	auto Call = SubstrateTypes::Call<SessionId, std::reference_wrapper<UGameplayActionBase>, int32>{
		8, 5, {0, std::ref(*ActionClass), TargetId}
	};
	SubmitExtrinsic(kagome::scale::encode(Call).value());
}

void UBlockchainSubsystem::DestroyActor(int32)
{
	unimplemented()
}

void UBlockchainSubsystem::SubmitExtrinsic(std::vector<uint8> EncodedCall)
{
	check(RpcClient != nullptr);
	check(RpcClient->IsConnected());
	FetchGenesisHash().Next([this, EncodedCall=MoveTemp(EncodedCall)](auto GenesisHash) mutable
	{
		FetchNextAccountNonce(SubstrateTypes::ALICE_SS58_ID).Next(
			[this, GenesisHash = MoveTemp(GenesisHash), EncodedCall = MoveTemp(EncodedCall)](const uint32 Nonce) mutable
			{
				AccumulatedNonce = FMath::Max(AccumulatedNonce, Nonce);
				auto Ext = SubstrateTypes::create_signed_extrinsic(GenesisHash, MoveTemp(EncodedCall),
				                                                   SubstrateTypes::ALICE_ACCOUNT_ID,
				                                                   AccumulatedNonce);
				AccumulatedNonce++;
				UE_LOG(LogBlockchain, Display, TEXT("Extrinsic: %s"),
				       ANSI_TO_TCHAR(kagome::common::hex_lower(Ext).data()));
				auto Params = UBlockchainMetodsFunctionLibrary::CreateSubmitAndWatchParameters(
					TArray{Ext.data(), static_cast<int>(Ext.size())});
				return CallMethod("author", "submitAndWatchExtrinsic", Params,
				                  {},
				                  {});
			});
	});
}

void UBlockchainSubsystem::ConnectTo(FString Host, const int32 Port, const FOnWebsocketConnected& OnConnected,
                                     const FOnWebsocketConnectionError& OnError)
{
	check(RpcClient == nullptr);
	check(!IsConnected());
	RpcClient = MakeUnique<FJsonRpcClient>();
	RpcClient->Connect(MoveTemp(Host), Port).Next(
		[this, OnConnected, OnError](TSharedPtr<TValueOrError<FEmptyVariantState, FString>>&& Result)
		{
			if (Result->HasError())
			{
				UE_LOG(LogBlockchain, Error, TEXT("%s %p"), *Result->GetError())
				OnError.ExecuteIfBound(Result->GetError());
			}
			else
			{
				UE_LOG(LogBlockchain, Display, TEXT("Connected successfully! %p"), this);
				(void)OnConnected.ExecuteIfBound();
			}
		});
}

void UBlockchainSubsystem::CallMethod(FString Module, FString Method, UVaRestJsonValue* Params,
                                      const FOnJsonRpcResponse& OnResponse,
                                      const FOnJsonRpcError& OnRpcError)
{
	check(RpcClient != nullptr);
	UE_LOG(LogBlockchain, Display, TEXT("Calling %s %s %p"), *Module, *Method, this);
	auto FutureResult = RpcClient->CallMethod(MoveTemp(Module), MoveTemp(Method), Params);

	FutureResult.Then(
		[this, OnResponse, OnRpcError](
		TFuture<TSharedPtr<TValueOrError<FJsonRpcClient::FCallMethodResponse, FString>>>&& FutureResult)
		{
			check(FutureResult.IsReady());
			auto const& Result = *FutureResult.Get();
			if (const TSharedPtr<FJsonValue>* ValuePtr = Result.TryGetValue(); ValuePtr != nullptr)
			{
				auto Value = NewObject<UVaRestJsonValue>(this);
				TSharedPtr<FJsonValue> ValuePtrCopy = *ValuePtr;
				Value->SetRootValue(ValuePtrCopy);
				OnResponse.ExecuteIfBound(Value);
			}
			else
			{
				(void)OnRpcError.ExecuteIfBound(Result.GetError());
			}
		});
}

bool UBlockchainSubsystem::IsConnected() const
{
	return RpcClient != nullptr && RpcClient->IsConnected();
}

TFuture<Blockchain::Metadata::Metadata> UBlockchainSubsystem::FetchMetadata() const
{
	unimplemented(); // see FetchGenesisHash
	return {};
}

TFuture<Blockchain::Common::FHash256> UBlockchainSubsystem::FetchGenesisHash() const
{
	auto VaRestSubsystem = GEngine->GetEngineSubsystem<UVaRestSubsystem>();
	return RpcClient->CallMethod("chain", "getBlockHash",
	                             VaRestSubsystem->ConstructJsonValueArray({
		                             VaRestSubsystem->ConstructJsonValueNumber(0)
	                             })).Next([](
		TSharedPtr<TValueOrError<FJsonRpcClient::FCallMethodResponse, FString>> const& ResponseRes)
		{
			if (const auto Response = ResponseRes->TryGetValue(); Response != nullptr)
			{
				const auto GenesisHashHex = Response->Get()->AsString();
				const auto GenesisHashBytes = kagome::common::unhexWith0x(TCHAR_TO_UTF8(*GenesisHashHex)).value();
				return kagome::scale::decode<Blockchain::Common::FHash256>(GenesisHashBytes).value();
			}
			checkNoEntry();
			return Blockchain::Common::FHash256{};
		});
}

TFuture<uint32> UBlockchainSubsystem::FetchNextAccountNonce(FAnsiStringView AccountSs58) const
{
	auto VaRestSubsystem = GEngine->GetEngineSubsystem<UVaRestSubsystem>();
	return RpcClient->CallMethod("system", "accountNextIndex",
	                             VaRestSubsystem->ConstructJsonValueArray({
		                             VaRestSubsystem->ConstructJsonValueString(AccountSs58.GetData())
	                             })).Next([](
		TSharedPtr<TValueOrError<FJsonRpcClient::FCallMethodResponse, FString>> const& ResponseRes) -> uint32
		{
			if (const auto Response = ResponseRes->TryGetValue(); Response != nullptr)
			{
				const auto Nonce = Response->Get()->AsNumber();
				return Nonce;
			}
			checkNoEntry();
			return 0;
		});
}

void UBlockchainSubsystem::RegisterBlockchainData(UBlockchainDataComponent* Component)
{
	KnownBlockchainDataComponents.Push(Component); 
}

