// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockchainMetodsFunctionLibrary.h"

#include "VaRestSubsystem.h"

#include "BlockchainDefines.hpp"
#include "Algo/Copy.h"
#include "Types/SubstrateTypes.h"
#include "Engine/Engine.h"

UVaRestJsonValue* UBlockchainMetodsFunctionLibrary::CreateEchoMethodParameters(FString Message)
{
	auto VaRestSubsystem = GEngine->GetEngineSubsystem<UVaRestSubsystem>();
	auto JsonMessage = VaRestSubsystem->ConstructJsonValueString(Message);
	auto ParamsArray = VaRestSubsystem->ConstructJsonValueArray({JsonMessage});
	return ParamsArray;
}


UBlockchainMetodsFunctionLibrary::Extrinsic
UBlockchainMetodsFunctionLibrary::CreateTransferExtrinsic(FString Receiver, int Amount, int32 Nonce)
{
	SubstrateTypes::AccountId CharlieId;
	{
		auto bytes = kagome::common::unhexWith0x(
			"0x90b5ab205c6974c9ea841be688864633dc9ca8a357843eeacf2314649965fe22").value();
		std::copy_n(bytes.begin(), CharlieId.size(), CharlieId.begin());
	}

	SubstrateTypes::Call<SubstrateTypes::AccountId, kagome::scale::CompactInteger> call{
		5, //client.get_module_idx("Balances"),
		0, //client.get_call_idx("Balances", "transfer"),
		std::make_tuple(CharlieId, 100500)
	};
	auto CallBytes = kagome::scale::encode(call).value();
	check(!"Need to fetch genesis hash here");
	auto ExtBytes = SubstrateTypes::create_signed_extrinsic({}, CallBytes, SubstrateTypes::ALICE_ACCOUNT_ID, Nonce);

	return Extrinsic{ExtBytes.data(), static_cast<int32>(ExtBytes.size())};
}

UVaRestJsonValue* UBlockchainMetodsFunctionLibrary::CreateSubmitAndWatchParameters(
	Extrinsic Extrinsic
)
{
	auto ExtrinsicHex = BytesToHex(Extrinsic.GetData(), Extrinsic.Num()).ToLower();
	auto VaRestSubsystem = GEngine->GetEngineSubsystem<UVaRestSubsystem>();
	auto JsonExtrinsicHex = VaRestSubsystem->ConstructJsonValueString("0x" + ExtrinsicHex);
	UE_LOG(LogBlockchain, Display, TEXT("Extrinsic: %s"), *ExtrinsicHex);
	auto ParamsArray = VaRestSubsystem->ConstructJsonValueArray({JsonExtrinsicHex});
	return ParamsArray;
}

FGameplayDataInstance UBlockchainMetodsFunctionLibrary::CreateDataInstance_Number(int32 Number)
{
	return FGameplayDataInstance{
		.Type = EDataType::Number,
		.Data {TInPlaceType<int32>{}, Number}
	};
}

FGameplayDataInstance UBlockchainMetodsFunctionLibrary::CreateDataInstance_String(FString const& String)
{
	auto AnsiString = StringCast<ANSICHAR>(*String);
	TArray<uint8> ByteString;
	ByteString.AddUninitialized(AnsiString.Length());
	for (int i = 0; i < AnsiString.Length(); ++i)
	{
		ByteString[i] = AnsiString.Get()[i];
	}
	return FGameplayDataInstance{
		.Type = EDataType::String,
		.Data {TInPlaceType<TArray<uint8>>{}, MoveTemp(ByteString)}
	};
}

FGameplayDataInstance UBlockchainMetodsFunctionLibrary::CreateDataInstance_Struct(FGameplayStructInstance Struct)
{
	return FGameplayDataInstance{
		.Type = EDataType::Structure,
		.Data {TInPlaceType<FGameplayStructInstance>{}, MoveTemp(Struct)}
	};
}
