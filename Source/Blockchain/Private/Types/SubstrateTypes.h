﻿#pragma once

#include "Containers/StringView.h"

THIRD_PARTY_INCLUDES_START
#pragma warning (disable: 4456)
#include <scale/scale.hpp>
#include <scale/buffer/hexutil.hpp>

extern "C" {
#include <schnorrkel/schnorrkel.h>
#include <blake2/blake2b.h>
}

THIRD_PARTY_INCLUDES_END

namespace SubstrateTypes
{
	using AccountIndex = kagome::scale::CompactInteger;

	using AccountId = std::array<uint8_t, 32>;
	using RawAddress = std::vector<uint8_t>;
	using Address32 = std::array<uint8_t, 32>;
	using Address20 = std::array<uint8_t, 20>;
	using MultiAddress = boost::variant<AccountId, /*compact codec*/AccountIndex, RawAddress, Address32, Address20>;

	const AccountId ALICE_ACCOUNT_ID = []()
	{
		auto bytes = kagome::common::unhex("d43593c715fdd31c61141abd04a99fd6822c8558854ccde39a5684e7a56da27d").value();
		BOOST_ASSERT(bytes.size() == SR25519_PUBLIC_SIZE);
		AccountId id;
		std::copy_n(bytes.begin(), id.size(), id.begin());
		return id;
	}();

	const FAnsiStringView ALICE_SS58_ID = "5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY";
	
	struct Ed25519Signature
	{
		std::array<uint8_t, ED25519_SIGNATURE_LENGTH> data;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const Ed25519Signature& sig)
	{
		return s << sig.data;
	}

	struct Sr25519Signature
	{
		std::array<uint8_t, SR25519_SIGNATURE_SIZE> data;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const Sr25519Signature& sig)
	{
		return s << sig.data;
	}

	struct EcdsaSignature
	{
		std::array<uint8_t, 65> data;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const EcdsaSignature& sig)
	{
		return s << sig.data;
	}

	using MultiSignature = boost::variant<Ed25519Signature, Sr25519Signature, EcdsaSignature>;

	const std::array<uint8_t, SR25519_SEED_SIZE> ALICE_SEED = []()
	{
		auto bytes = kagome::common::unhex("e5be9a5092b81bca64be81d212e7f2f9eba183bb7a90954f7b76361f6edb5c0a").value();
		BOOST_ASSERT(bytes.size() == SR25519_SEED_SIZE);
		std::array<uint8_t, SR25519_SEED_SIZE> secret{};
		std::copy_n(bytes.begin(), secret.size(), secret.begin());
		return secret;
	}();

	struct CheckSpecVersion
	{
		uint32_t spec_version;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const CheckSpecVersion& ext)
	{
		return s;
	}

	struct CheckTxVersion
	{
		uint32_t format_version;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const CheckTxVersion& ext)
	{
		return s;
	}

	struct CheckGenesis
	{
		std::array<uint8_t, 32> genesis_hash;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const CheckGenesis& ext)
	{
		return s;
	}

	struct Immortal
	{
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const Immortal& m)
	{
		return s;
	}

	struct Mortal
	{
		uint64_t period;
		uint64_t phase;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const Mortal& m)
	{
		return s;
	}

	using Era = boost::variant<Immortal, Mortal>;

	struct CheckMortality
	{
		Era mortality;
		std::array<uint8_t, 32> block_hash;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const CheckMortality& ext)
	{
		return s << ext.mortality;
	}

	using Nonce = kagome::scale::CompactInteger; // uint32_t; // transaction index
	struct CheckNonce
	{
		Nonce nonce; // compact codec
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const CheckNonce& ext)
	{
		return s << ext.nonce;
	}

	struct CheckWeight
	{
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const CheckWeight& ext)
	{
		return s;
	}

	using Balance = kagome::scale::CompactInteger; // boost::multiprecision::uint128_t;
	struct ChargeTransactionPayment
	{
		Balance tip; // compact codec
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
	Stream& operator<<(Stream& s, const ChargeTransactionPayment& ext)
	{
		return s << ext.tip;
	}

	using SignedExtra = std::tuple<
		CheckSpecVersion,
		CheckTxVersion,
		CheckGenesis,
		CheckMortality,
		CheckNonce,
		CheckWeight,
		ChargeTransactionPayment>;

	template <typename... Args>
	struct Call
	{
		uint8_t module;
		uint8_t call;
		std::tuple<Args...> args;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>, typename... Args>
	Stream& operator<<(Stream& Encoder, const Call<Args...>& Call)
	{
		return Encoder << Call.module << Call.call << Call.args;
	}

	template <typename... Args>
	struct SignedExtrinsic
	{
		uint8_t type_version = 0b1000'0100;
		// 0... - unsigned, 1... - signed, ... - version (latest is 4 at the moment of writing this code)
		std::tuple<MultiAddress, MultiSignature, SignedExtra> signature;
		Call<Args...> function;
	};

	template <typename... Args>
	struct UnsignedExtrinsic
	{
		uint8_t type_version = 0b0000'0100;
		// 0... - unsigned, 1... - signed, ... - version (latest is 4 at the moment of writing this code)
		Call<Args...> function;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>, typename... Args>
	Stream& operator<<(Stream& s, const SignedExtrinsic<Args...>& ext)
	{
		std::cout << kagome::common::hex_lower(kagome::scale::encode(ext.type_version).value()) << "||";
		std::cout << kagome::common::hex_lower(kagome::scale::encode(std::get<0>(ext.signature)).value()) << "|";
		std::cout << kagome::common::hex_lower(kagome::scale::encode(std::get<1>(ext.signature)).value()) << "|";
		std::cout << kagome::common::hex_lower(kagome::scale::encode(std::get<2>(ext.signature)).value()) << "||";
		std::cout << kagome::common::hex_lower(kagome::scale::encode(ext.function.module).value()) << "|";
		std::cout << kagome::common::hex_lower(kagome::scale::encode(ext.function.call).value()) << "|";
		std::cout << kagome::common::hex_lower(kagome::scale::encode(ext.function.args).value()) << "||";

		return s << ext.type_version << ext.signature << ext.function;
	}

	template <class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>, typename... Args>
	Stream& operator<<(Stream& s, const UnsignedExtrinsic<Args...>& ext)
	{
		return s << ext.type_version << ext.function;
	}

	Sr25519Signature
	sign_extrinsic(std::array<uint8_t, SR25519_SEED_SIZE> seed, std::vector<uint8_t> payload)
	{
		std::array<uint8_t, SR25519_KEYPAIR_SIZE> keypair{};
		sr25519_keypair_from_seed(keypair.data(), seed.data());

		Sr25519Signature signature{};
		sr25519_sign(signature.data.data(), keypair.data() + SR25519_SECRET_SIZE, keypair.data(), payload.data(),
		             payload.size());
		std::cout << "payload: " << kagome::common::hex_lower(payload) << "\n";
		std::cout << "public: " << kagome::common::hex_lower(keypair).substr(SR25519_SECRET_SIZE, SR25519_KEYPAIR_SIZE)
			<< "\n";
		std::cout << "secret: " << kagome::common::hex_lower(keypair).substr(0, SR25519_SECRET_SIZE) << "\n";
		std::cout << "signature: " << kagome::common::hex_lower(signature.data) << "\n";
		return signature;
	}

	auto create_signed_extrinsic(std::array<uint8_t, 32> genesis_hash, const std::vector<uint8_t>& enc_call,
	                             MultiAddress address, uint32_t nonce)
	{
		SignedExtra extras{
			std::make_tuple(
				CheckSpecVersion{
					.spec_version = 100
				},
				CheckTxVersion{
					.format_version = 1
				},
				CheckGenesis{
					.genesis_hash = genesis_hash
				},
				CheckMortality{
					.mortality = Immortal{},
					.block_hash = genesis_hash
				},
				CheckNonce{
					.nonce = nonce
				},
				CheckWeight{},
				ChargeTransactionPayment{
					.tip = 0
				}
			)
		};
		auto extra = std::make_tuple(uint8_t{0}, kagome::scale::CompactInteger{nonce},
		                             kagome::scale::CompactInteger{0});
		auto additional_extra = std::make_tuple<uint32_t, uint32_t, std::array<uint8_t, 32>&, std::array<uint8_t, 32>&>(
			100, 1, genesis_hash, genesis_hash);
		std::vector<uint8_t> raw_payload;
		{
			auto e = kagome::scale::encode(std::make_tuple(extra, additional_extra)).value();
			raw_payload.resize(enc_call.size() + e.size());
			std::copy_n(enc_call.begin(), enc_call.size(), raw_payload.begin());
			std::copy_n(e.begin(), e.size(), raw_payload.begin() + enc_call.size());
		}
		std::vector<uint8_t> payload;
		if (raw_payload.size() > 256)
		{
			payload.resize(32);
			blake2b(payload.data(), 32, nullptr, 0, raw_payload.data(), raw_payload.size());
		}
		else
		{
			payload = std::move(raw_payload);
		}

		auto signature = MultiSignature{sign_extrinsic(ALICE_SEED, payload)};

		UE_LOG(LogBlockchain, Display, TEXT("Encoded extra: %s"), ANSI_TO_TCHAR(kagome::common::hex_lower(kagome::scale::encode(extra).value()).data()));
		UE_LOG(LogBlockchain, Display, TEXT("Encoded call: %s"), ANSI_TO_TCHAR(kagome::common::hex_lower(enc_call).data()));
		auto enc_ext = kagome::scale::encode((uint8_t)0b1000'0100, std::make_tuple(address, signature, extra)).value();
		enc_ext.insert(enc_ext.end(), enc_call.begin(), enc_call.end());

		return kagome::scale::encode(enc_ext).value();
	}
}
