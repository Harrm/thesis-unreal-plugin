﻿#pragma once

#include <string>

THIRD_PARTY_INCLUDES_START
#pragma warning(disable: 4456)
#include <boost/variant.hpp>
#include <boost/optional.hpp>
THIRD_PARTY_INCLUDES_END

namespace Blockchain::Metadata
{

	enum StorageEntryModifier
	{
		Optional,
		Default
	};

	enum StorageHasher
	{
		Blake2_128,
		Blake2_256,
		Blake2_128Concat,
		Twox128,
		Twox256,
		Twox64Concat,
		Identity
	};

	using PlainStorageEntry = std::string;

	struct MapStorageEntry
	{
		StorageHasher hasher;
		std::string key;
		std::string value;
		bool unused;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, MapStorageEntry& meta)
	{
		uint8_t hasher{};
		s >> hasher >> meta.key >> meta.value >> meta.unused;
		meta.hasher = static_cast<StorageHasher>(hasher);
		return s;
	}

	struct DoubleMapStorageEntry
	{
		StorageHasher hasher1;
		std::string key1;
		std::string key2;
		std::string value;
		StorageHasher hasher2;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, DoubleMapStorageEntry& meta)
	{
		uint8_t hasher{}, hasher2{};
		s >> hasher >> meta.key1 >> meta.key2 >> meta.value >> hasher2;
		meta.hasher1 = static_cast<StorageHasher>(hasher);
		meta.hasher2 = static_cast<StorageHasher>(hasher2);
		return s;
	}

	using StorageEntryType = boost::variant<PlainStorageEntry, MapStorageEntry, DoubleMapStorageEntry>;

	struct StorageEntryMetadata
	{
		std::string name;
		StorageEntryModifier modifier;
		StorageEntryType type;
		std::vector<uint8_t> value;
		std::vector<std::string> documentation;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, StorageEntryMetadata& meta)
	{
		uint8_t modifier;
		s >> meta.name >> modifier >> meta.type >> meta.value >> meta.documentation;
		meta.modifier = (StorageEntryModifier)modifier;
		return s;
	}

	struct StorageMetadata
	{
		std::string prefix;
		std::vector<StorageEntryMetadata> entries;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, StorageMetadata& meta)
	{
		s >> meta.prefix >> meta.entries;
		return s;
	}

	struct FunctionArgumentMetadata
	{
		std::string name;
		std::string type;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, FunctionArgumentMetadata& meta)
	{
		s >> meta.name >> meta.type;
		return s;
	}

	struct FunctionMetadata
	{
		std::string name;
		std::vector<FunctionArgumentMetadata> arguments;
		std::vector<std::string> documentation;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, FunctionMetadata& meta)
	{
		s >> meta.name >> meta.arguments >> meta.documentation;
		return s;
	}

	struct EventMetadata
	{
		std::string name;
		std::vector<std::string> arguments;
		std::vector<std::string> documentation;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, EventMetadata& meta)
	{
		s >> meta.name >> meta.arguments >> meta.documentation;
		return s;
	}

	struct ModuleConstantMetadata
	{
		std::string name;
		std::string type;
		std::vector<uint8_t> value;
		std::vector<std::string> documentation;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, ModuleConstantMetadata& meta)
	{
		s >> meta.name >> meta.type >> meta.value >> meta.documentation;
		return s;
	}

	struct ErrorMetadata
	{
		std::string name;
		std::vector<std::string> documentation;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, ErrorMetadata& meta)
	{
		s >> meta.name >> meta.documentation;
		return s;
	}

	struct ModuleMetadata
	{
		std::string name;
		boost::optional<StorageMetadata> storage;
		boost::optional<std::vector<FunctionMetadata>> calls;
		boost::optional<std::vector<EventMetadata>> events;
		std::vector<ModuleConstantMetadata> constants;
		std::vector<ErrorMetadata> errors;
		uint8_t index;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, ModuleMetadata& meta)
	{
		s >> meta.name >> meta.storage >> meta.calls >> meta.events >> meta.constants >> meta.errors >> meta.index;
		return s;
	}

	struct ExtrinsicMetadata
	{
		uint8_t version;
		std::vector<std::string> signed_extensions;
	};

	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, ExtrinsicMetadata& ext_meta)
	{
		s >> ext_meta.version >> ext_meta.signed_extensions;
		return s;
	}

	struct Metadata
	{
		static constexpr uint32_t kMagicNumber = 0x6174656D; // decoded "meta"
		uint8_t version;
		std::vector<ModuleMetadata> modules_metadata;
		ExtrinsicMetadata ext_metadata;
	};

	/**
	 * @brief decodes buffer object from stream
	 * @tparam Stream input stream type
	 * @param s stream reference
	 * @param m value to decode
	 * @return reference to stream
	 */
	template <class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
	Stream& operator>>(Stream& s, Metadata& m)
	{
		uint32_t magic{};
		s >> magic >> m.version >> m.modules_metadata >> m.ext_metadata;
		BOOST_ASSERT(magic == m.kMagicNumber);
		return s;
	}

}