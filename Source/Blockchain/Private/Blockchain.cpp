// Copyright Epic Games, Inc. All Rights Reserved.

#include "Blockchain.h"

#include "WebSocketsModule.h"

#include "BlockchainDefines.hpp"

#define LOCTEXT_NAMESPACE "FBlockchainModule"

void FBlockchainModule::StartupModule()
{
	FModuleManager::Get().LoadModuleChecked(TEXT("WebSockets"));
}

void FBlockchainModule::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FBlockchainModule, Blockchain)
DEFINE_LOG_CATEGORY(LogBlockchain)
