﻿#include "JsonRpcClient.h"

#include "WebSocketsModule.h"
#include "IWebSocket.h"
#include "Dom/JsonObject.h"
#include "Serialization/JsonTypes.h"
#include "Dom/JsonValue.h"
#include "Templates/SharedPointer.h"

THIRD_PARTY_INCLUDES_START
#pragma warning (disable: 4583)
#pragma warning (disable: 4582)
#include "jsonrpc-lean/client.h"
#include "scale/scale.hpp"
THIRD_PARTY_INCLUDES_END

#include "BlockchainDefines.hpp"

FJsonRpcClient::FJsonRpcClient()
{
	FormatHandler = MakeShared<jsonrpc::JsonFormatHandler>();
	RpcClient = MakeShared<jsonrpc::Client>(*FormatHandler);
}

FJsonRpcClient::~FJsonRpcClient()
{
	for (auto& Promise : CallMethodResultPromises)
	{
		if (Promise)
			Promise->EmplaceValue(
				MakeShared<FCallMethodResult>(MakeError(FString{TEXT("Promise never received value!")})));
	}
}

TFuture<TSharedPtr<FJsonRpcClient::FConnectResult>> FJsonRpcClient::Connect(const FString Host, const int32 Port)
{
	const auto Uri = FString::Printf(TEXT("ws://%s:%hu"), *Host, Port);
	WebSocket = FWebSocketsModule::Get().CreateWebSocket(Uri);

	check(WebSocket != nullptr);

	auto Promise = MakeShared<TPromise<TSharedPtr<FConnectResult>>>();
	auto Future = Promise->GetFuture();

	WebSocket->OnConnected().AddLambda([this, Promise]()
	{
		UE_LOG(LogBlockchain, Display, TEXT("Connected web socket"));
		check(WebSocket->IsConnected());
		Promise->SetValue(MakeShared<FConnectResult>(MakeValue(FEmptyVariantState{})));
	});
	WebSocket->OnConnectionError().AddLambda([Promise](const FString& Error) -> void
	{
		UE_LOG(LogBlockchain, Error, TEXT("Connection error in web socket: %s"), *Error);
		Promise->SetValue(MakeShared<FConnectResult>(MakeError(Error)));
	});
	WebSocket->OnClosed().AddLambda([this](const int32 StatusCode, const FString& Reason, bool) -> void
	{
		UE_LOG(LogBlockchain, Display, TEXT("Connection closed in web socket: status %d, reason: %s"), StatusCode,
		       *Reason);
	});
	WebSocket->OnMessage().AddLambda([this](const FString& Message) -> void
	{
		UE_LOG(LogBlockchain, Display, TEXT("Received a message in web socket: %s"), *Message);
		ProcessResponse(Message);
	});
	WebSocket->OnRawMessage().AddLambda([](const void*, const SIZE_T Size, const SIZE_T BytesRemaining) -> void
	{
		UE_LOG(LogBlockchain, VeryVerbose, TEXT("Received a raw message in web socket: size: %d, remaining: %d"), Size,
		       BytesRemaining);
	});
	WebSocket->OnMessageSent().AddLambda([](const FString& MessageString) -> void
	{
		UE_LOG(LogBlockchain, Display, TEXT("Sent a message in web socket: %s"), *MessageString);
	});

	WebSocket->Connect();

	return Future;
}

TFuture<TSharedPtr<FJsonRpcClient::FCallMethodResult>> FJsonRpcClient::CallMethod(
	const FString Module, const FString Method, UVaRestJsonValue* Params)
{
	check(WebSocket != nullptr);
	ensureAlways(WebSocket->IsConnected());

	const auto MethodName = FString::Printf(TEXT("%s_%s"), *Module, *Method);
	const std::string UtfMethodName{TCHAR_TO_UTF8(*MethodName)};
	check(RpcClient.IsValid());

	jsonrpc::Request::Parameters RequestParams;
	if (Params != nullptr)
	{
		for (auto Value : Params->AsArray())
		{
			RequestParams.push_back(ConvertFJsonToJsonRpcJson(*Value->GetRootValue()));
		}
	}
	auto Writer = FormatHandler->CreateWriter();
	const int32 Id = NextCallId++;
	jsonrpc::Request::Write(UtfMethodName, RequestParams, jsonrpc::Value{Id}, *Writer);
	auto JsonRequest = Writer->GetData();

	auto Promise = MakeUnique<TPromise<TSharedPtr<FCallMethodResult>>>();
	auto Future = Promise->GetFuture();
	if (CallMethodResultPromises.Num() <= Id)
	{
		CallMethodResultPromises.AddZeroed(Id - CallMethodResultPromises.Num() + 1);
	}
	CallMethodResultPromises[Id] = MoveTemp(Promise);
	if (WebSocket->IsConnected())
	{
		WebSocket->Send(JsonRequest->GetData());
	}
	else
	{
		CallMethodResultPromises[Id]->EmplaceValue(
			MakeShared<FCallMethodResult>(MakeError(TEXT("Socket is not connected!"))));
		CallMethodResultPromises.Reset();
	}
	return Future;
}

void AddEventHandler(FString EventKey, TFunction<void(UVaRestJsonValue*)> Handler)
{
	
}

void FJsonRpcClient::ProcessResponse(const FString& Message) const
{
	auto Reader = FormatHandler->CreateReader(TCHAR_TO_UTF8(*Message));
	try
	{
		jsonrpc::Response Response = Reader->GetResponse();
		if (const auto ResponseId = Response.GetId().AsInteger32(); Response.IsFault())
		{
			try
			{
				Response.ThrowIfFault();
			}
			catch (jsonrpc::Fault& f)
			{
				CallMethodResultPromises[ResponseId]->
					EmplaceValue(
						MakeShared<FCallMethodResult>(MakeError(ANSI_TO_TCHAR(f.what()))));
				CallMethodResultPromises[ResponseId].Reset();
			}
		}
		else
		{
			auto FJson = ConvertJsonRpcJsonToFJsonValue(Response.GetResult());
			CallMethodResultPromises[ResponseId]->EmplaceValue(MakeShared<FCallMethodResult>(MakeValue(FJson)));
			CallMethodResultPromises[ResponseId].Reset();
		}
	}
	catch (jsonrpc::InvalidRequestFault&)
	{
		UE_LOG(LogBlockchain, Display, TEXT("Received JSON RPC update: %s"), *Message);
	}
}

void FJsonRpcClient::Close()
{
	check(WebSocket != nullptr);
	WebSocket->Close();
}

bool FJsonRpcClient::IsConnected() const
{
	return WebSocket != nullptr && WebSocket->IsConnected();
}

jsonrpc::Value FJsonRpcClient::ConvertFJsonToJsonRpcJson(const FJsonValue& Value)
{
	switch (Value.Type)
	{
	case EJson::None: return jsonrpc::Value{};
	case EJson::Null: return jsonrpc::Value{};
	case EJson::String: return jsonrpc::Value{TCHAR_TO_UTF8(*Value.AsString())};
	case EJson::Number: return jsonrpc::Value{static_cast<int64_t>(Value.AsNumber())};
	case EJson::Boolean: return jsonrpc::Value{Value.AsBool()};
	case EJson::Array:
		{
			auto Arr1 = Value.AsArray();
			jsonrpc::Value::Array Arr2;
			Arr2.reserve(Arr1.Num());
			for (auto ValuePtr : Arr1)
			{
				Arr2.push_back(ConvertFJsonToJsonRpcJson(*ValuePtr));
			}
			return Arr2;
		}
	case EJson::Object:
		{
			auto Obj1 = Value.AsObject();
			jsonrpc::Value::Struct Obj2;
			for (auto& KeyValue : Obj1->Values)
			{
				Obj2[TCHAR_TO_UTF8(*KeyValue.Key)] = ConvertFJsonToJsonRpcJson(*KeyValue.Value);
			}
			return Obj2;
		}
	default: return jsonrpc::Value{};
	}
}

TSharedPtr<FJsonValue> FJsonRpcClient::ConvertJsonRpcJsonToFJsonValue(const jsonrpc::Value& Value)
{
	TSharedPtr<FJsonValue> NewValue{};
	switch (Value.GetType())
	{
	case jsonrpc::Value::Type::NIL:
		NewValue = MakeShared<FJsonValueNull>();
		break;
	case jsonrpc::Value::Type::ARRAY:
		{
			TArray<TSharedPtr<FJsonValue>> Array;
			for (auto& Arg : Value.AsArray())
			{
				Array.Push(ConvertJsonRpcJsonToFJsonValue(Arg));
			}
			NewValue = MakeShared<FJsonValueArray>(Array);
			break;
		}
	case jsonrpc::Value::Type::DOUBLE:
		NewValue = MakeShared<FJsonValueNumber>(Value.AsDouble());
		break;
	case jsonrpc::Value::Type::INTEGER_32:
		NewValue = MakeShared<FJsonValueNumber>(Value.AsInteger32());
		break;
	case jsonrpc::Value::Type::INTEGER_64:
		NewValue = MakeShared<FJsonValueNumber>(Value.AsInteger64());
		break;
	case jsonrpc::Value::Type::STRING:
		NewValue = MakeShared<FJsonValueString>(Value.AsString().c_str());
		break;
	case jsonrpc::Value::Type::STRUCT:
		{
			TSharedPtr<FJsonObject> Object = MakeShared<FJsonObject>();
			for (auto&& [Name, Field] : Value.AsStruct())
			{
				Object->SetField(Name.c_str(), ConvertJsonRpcJsonToFJsonValue(Field));
			}
			NewValue = MakeShared<FJsonValueObject>(Object);
			break;
		}
	case jsonrpc::Value::Type::BOOLEAN:
		NewValue = MakeShared<FJsonValueBoolean>(Value.AsBoolean());
		break;
	case jsonrpc::Value::Type::BINARY:
	case jsonrpc::Value::Type::DATE_TIME:
		unimplemented();
	}
	return NewValue;
}
