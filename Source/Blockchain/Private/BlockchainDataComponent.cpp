﻿#include "BlockchainDataComponent.h"

#include "BlockchainSubsystem.h"
#include "Engine/World.h"

void UBlockchainDataComponent::OnRegister()
{
	auto BlockchainSubsystem = GetWorld()->GetGameInstance()->GetSubsystem<UBlockchainSubsystem>();
	BlockchainSubsystem->RegisterBlockchainData(this);
}


void UBlockchainDataComponent::SetTrackedActorId(const int Id)
{
	TrackedActorId = Id;
}

void UBlockchainDataComponent::OnParamUpdate_Implementation(UParamEntry* Param)
{
	
}
